package com.atlassian.platform.maven;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.repository.RepositorySystem;
import org.dom4j.Node;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class PlatformPom {
    private final RepositorySystem repositorySystem;
    private final File pomFile;

    public PlatformPom(RepositorySystem repositorySystem, File pomFile) {
        this.repositorySystem = repositorySystem;
        this.pomFile = pomFile;
    }

    public List<Artifact> dependencies() throws MojoExecutionException {
        try (InputStream in = new FileInputStream(pomFile)) {
            MavenXPath docXPath = new MavenXPath(in);

            // Read version variables
            Map<String, String> versions = new HashMap<>();
            List<Node> versionNodes = docXPath.selectNodes("/xmlns:project/xmlns:properties/*");
            for (Node v : versionNodes) {
                versions.put(v.getName(), v.getText());
            }

            // Read dependencies, resolve version veriable and create POJO
            List<Artifact> result = new ArrayList<>();
            List<Node> deps = docXPath.selectNodes("/xmlns:project/xmlns:dependencyManagement/xmlns:dependencies/*");
            for (Node node : deps) {
                MavenXPath nodeXPath = new MavenXPath(node);
                String group = nodeXPath.valueOf("./xmlns:groupId/text()");
                String artifact = nodeXPath.valueOf("./xmlns:artifactId/text()");

                String version = nodeXPath.valueOf("./xmlns:version/text()").trim();
                if (version.startsWith("$")) {
                    version = versions.get(version.substring(2, version.length() - 1));
                }

                result.add(repositorySystem.createArtifact(group, artifact, version, "jar"));
            }

            // done...
            return result;
        } catch (Exception e) {
            throw new MojoExecutionException("Failed to parse platform-poms", e);
        }
    }
}
